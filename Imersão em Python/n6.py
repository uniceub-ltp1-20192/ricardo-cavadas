def amigos():
	friends = ['ricardo', 'josé', 'joão', 'maria']
	print(friends[0].title())
	print(friends[1].title())
	print(friends[2].title())
	print(friends[-1].title())
	
	for i in range(len(friends)):
		print ("Bem vindo " + friends[i].title() + "! Esse é seu grupo de amigos!" )

def veículos():
	cars = ['ferrari', 'lamborghini', 'porsche', 'bmw', 'audi']
	print('\nA ' + cars[0] + ' 488 spider é o modelo mais bonito!')
	print('A fábrica da ' + cars[1] + ' na itália é impressionante!')
	print('Eu ainda vou ter um ' + cars[2] + ' na minha garagem.')
	print('Meu irmão tem uma ' + cars[3].upper() + ' serie 7 2019.')
	print('Na Alemanha você vê ' + cars[-1].title() + ' o tempo inteiro.')

amigos()

veículos()