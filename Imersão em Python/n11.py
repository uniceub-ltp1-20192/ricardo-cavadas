def viagens():
	lugares = ["canadá", "marrocos", "austrália", "japão", "noruega"]
	print('Lista original: ')
	print(lugares)
	print('Os primeiros 3 itens da lista são: ', lugares[0:3])
	print('Os últimos 3 itens da lista são: ', lugares[2:5])
	print('Os 3 itens do meio da lista são: ', lugares[1:4])

def pizza():
	pizza_hut = ['calabresa', 'portuguesa', 'quatro queijos', 'frango', 'marguerita', 'peperoni', 'chocolate']
	pizza_hot_paraguai = pizza_hut[:]
	pizza_hut.append('milho')
	pizza_hot_paraguai.append('lombo')
	print('\nSabores da pizza hut: ', pizza_hut)
	print('\nSabores pizza paraguaia: ', pizza_hot_paraguai)

	print('\nMenu :\n')
	for i in pizza_hut:
		print(i)

viagens()
pizza()

