def lugares():
	cidades = ['new york', 'rio de janeiro', 'brasília', 'paris', 'bruxelas', 'amsterdam', 'barcelona']
	print('Temos uma lista com', len(cidades), 'cidades.')
	print(cidades)
	print(sorted(cidades))
	print(sorted(cidades, reverse=True))
	print(cidades)
	cidades.sort()
	print(cidades)
	cidades.sort(reverse=True)
	print(cidades)
	cidades.reverse()
	print(cidades)

lugares()