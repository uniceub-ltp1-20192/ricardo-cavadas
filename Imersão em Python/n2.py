# -*- coding: utf-8 -*-

def total():
	quantidade_cebolas = int(5)
	preco_cebola = int(2)
	print("Você irá pagar", quantidade_cebolas*preco_cebola, "reais pelas cebolas.\n")

total()

# exercício 2.2
cebolas = int(200)
cebolas_na_caixa = int(50)
espaco_caixa = int(4)
caixas = int(46)
# cebolas que ainda não tem caixa
cebolas_fora_da_caixa = cebolas - cebolas_na_caixa
#caixas vazias que ainda cabem cebolas
caixas_vazias = int(caixas - (cebolas_na_caixa/espaco_caixa))
#total de caixas necessário para encaixotar todas as cebolas
caixas_necessarias = int(cebolas_fora_da_caixa / espaco_caixa)
print("Existem", cebolas_na_caixa, "cebolas encaixotadas")
print("Existem", cebolas_fora_da_caixa, "cebolas sem caixa")
print("Em cada caixa cabem", espaco_caixa, "cebolas")
print("Ainda temos,", caixas_vazias, "caixas vazias")
print("Então, precisamos de", caixas_necessarias, "caixas para empacotar todas as cebolas")
