def lista():
	for i in range(1,21):
		print(i)

	numeros = list(range(1,1000001))
	maximo = max(numeros)
	minimo = min(numeros)
	soma= sum(numeros)
	print(minimo, maximo, soma)
	
def impares():
	for i in range(1,21,2):
		print(i)

def multiplos():
	for i in range(3, 1000, 3):
		print(i)
def cubos():
	for i in range(1,101):
		print(i**3)

lista()
impares()
multiplos()
cubos()

#compreensão de lista
cubos1 = [i**3 for i in range(1,101)]
print(cubos1)