#exercício 5.1
num = float(input("Digite um número: "))
if num % 2 == 0:
	print("O número é par!")
else:
	print("O número é ímpar!")

#exercício 5.2

def main():
    
    n = int(input("Digite um número maior que 0: "))

    divisores = 0

    # conta o número de divisores entre 1 e n
    for i in range(1,n+1):
        if n % i == 0:
            divisores += 1 

    # imprima mensagem
    if divisores == 2:
        print(n, "é primo")
    else:
        print(n, "não é primo")

main()
