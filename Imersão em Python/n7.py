def jantar():
	pessoas = ['jesse', 'walter', 'cebola']
	print('Temos', len(pessoas), 'convidados')
	print('Boa noite ' + pessoas[0].title() + '! Você é meu convidado para o jantar!')
	print('Olá ' + pessoas[1].title() + ', gostaria da sua presença hoje à noite para jantar!')
	print( pessoas[2].title() + ', espero você hoje em minha casa!')
#removendo um convidade e incluindo outro
	pessoas_atualizado = pessoas.pop(0)
	pessoas.insert(0, 'ceboludo')
	print('\nBoa noite ' + pessoas[0].title() + '! Você é o substituto para o jantar!')
	print('Olá ' + pessoas[1].title() + ', gostaria da sua presença hoje à noite para jantar!')
	print( pessoas[2].title() + ', espero você hoje em minha casa!')
#mensagem para o excluído	
	print("\nQue pena " + pessoas_atualizado.title() + ", você não pode comparecer... Fica pra próxima!")
#inserindo mais 3 pessoas na lista 
	print("\nPessoal, encontrei uma mesa de jantar maior! Vou chamar mais 3 pessoas:")
	pessoas.insert(0, 'hank')
	pessoas.insert(1, 'skyler')
	pessoas.append('heisenberg')
	print('Agora temos', len(pessoas), 'convidados')
	print('\nBoa noite ' + pessoas[0].title() + '! Jantar hj, vc que paga!')
	print('Olá ' + pessoas[1].title() + ', gostaria da sua presença hoje à noite para jantar!')
	print( pessoas[2].title() + ', espero você hoje em minha casa!')
	print('Fala ' + pessoas[3].title() + '! Você escolhe o menu do jantar de hoje.')
	print('Eae ' + pessoas[4].title() + '! Jantar hoje às 20:00 horas.')
	print(pessoas[5].title() + ', você será o cozinheiro hoje.')
#reduzindo numero de convidados para 2
	print('\nGalera, a mesa maior que comprei não vai chegar a tempo... vou convidar somente 2 de vocês para o jantar de hoje.')
	pessoas_atualizado1 = pessoas.pop()
	print("\nVocê foi removido do jantar, " + pessoas_atualizado1.title())
	pessoas_atualizado2 = pessoas.pop()
	print("Você foi desconvidado, " + pessoas_atualizado2.title())
	pessoas_atualizado3 = pessoas.pop()
	print("Você não pode ir hoje, " + pessoas_atualizado3.title())
	pessoas_atualizado4 = pessoas.pop()
	print(pessoas_atualizado4.title() + ", desculpa, fica pra próxima!\n\n")
#confirmando quem ainda está convidado
	print('Sobraram', len(pessoas), 'convidados.')
	print(pessoas[0].title() + ", jantar hoje ainda está de pé, viu?")
	print(pessoas[1].title() + ", espero você hoje à noite!")
#removendo todos da lista
	del pessoas[1]
	del pessoas[0]
	print(pessoas)

jantar()


