from Validador.validador import Validador
from Dados.dados import Dados
from Entidades.moto import Moto

class Menu:
    @staticmethod
    def menuPrincipal():
        print("""
            0 - Sair
            1 - Consultar
            2 - Inserir
            3 - Alterar
            4 - Deletar 
            """)
        return Validador.validar("[0-4]",
        """Opção do menu deve estar entre {}""", 
        """Opção {} é válida """)

    @staticmethod
    def menuConsultar():
        print("""
            0 - Voltar
            1 - Consultar por identificador
            2 - Consultar por propriedade
            """)
        return Validador.validar("[0-2]",
        """Opção do menu deve estar entre {}""", 
        """Opção {} é válida """)

    @staticmethod
    def iniciarMenu():
        opMenu = ""
        d = Dados()
        while opMenu != "0":
            opMenu = Menu.menuPrincipal()
            if opMenu == "1":
                while opMenu != "0":
                    opMenu = Menu.menuConsultar()
                    if opMenu == "1":
                        print("Entrou no menu Consultar por identificador")
                        retorno = Menu.menuBuscaPorIdentificador(d)
                        if retorno != None:
                            print(retorno)
                        else:
                            print("Não encontrado")
                    elif opMenu == "2":
                        Menu.menuBuscaPorAtributo(d)
                        print("Entrou no menu Consultar por propriedade")
                    elif opMenu == "0":
                        print("Saindo")
                    else:
                        print("Digite uma opcao valida!")  
                opMenu = ""            
            elif opMenu == "2":
                print("Entrei em Inserir")
                Menu.menuInserir(d)
                opMenu = ""
            elif opMenu == "3":
                print("Entrei em Alterar")
                while opMenu != "0":
                    opMenu = Menu.menuConsultar()
                    if opMenu == "1":
                        retorno = Menu.menuBuscaPorIdentificador(d)
                        if retorno != None:
                            Menu.menuAlterar(retorno,d)
                        else:
                            print("Nao encontrado")
                    elif opMenu == "2":
                        print("Entrou no menu Consultar por identificador")
                    elif opMenu == "0":
                        print("Saindo")
                    else:
                        print("Digite uma opcao valida!")
                opMenu = "" 
            elif opMenu == "4":
                print("Entrei em Deletar")
                while opMenu != "0":
                    opMenu = Menu.menuConsultar()
                    if opMenu == "1":
                        retorno = Menu.menuBuscaPorIdentificador(d)
                        Menu.menuDeletar(d, retorno)
                    elif opMenu == "2":
                        Menu.menuBuscaPorAtributo(d)
                    elif opMenu == "0":
                        print("Saindo")
                    else:
                        print("Digite uma opcao valida!")
                opMenu = "" 

    @staticmethod
    def menuBuscaPorIdentificador(d):
        retorno = d.buscarPorIdentificador(
            Validador.validar('\d+','',''))
        return retorno

    @staticmethod
    def menuBuscaPorAtributo(d):
        retorno = d.buscarPorAtributo(
            input("Informe uma montadora: "))
        print(retorno)

    @staticmethod
    def menuInserir(d):
        moto = Moto()
        moto.montadora = input("Informe uma montadora: ")
        moto.modelo = input("Informe um modelo: ")
        moto.ano = input("Informe um ano: ")
        moto.cor = input("Informe um cor: ")
        d.inserirDado(moto)

    @staticmethod
    def menuAlterar(retorno,d):
        print(retorno)
        retorno.montadora = Validador.validarValorEntrada(retorno.montadora, "Informe uma montadora: ")
        retorno.modelo = Validador.validarValorEntrada(retorno.modelo, "Informe um modelo: ")
        retorno.ano = Validador.validarValorEntrada(retorno.ano, "Informe um ano: ")
        retorno.cor = Validador.validarValorEntrada(retorno.cor, "Informe uma cor: ")
        
        d.alterarDado(retorno)

    @staticmethod
    def menuDeletar(d, entidade):
        print(entidade)
        resposta = input("""Deseja deletar? 
        S - Sim
        N - Nao
        
        Re: """)
        if(resposta == "S" or resposta == "s"):
            d.deletar(entidade)