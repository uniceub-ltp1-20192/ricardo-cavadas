def loja():
	a, b = 0, 0
	while True: 
		try: 
			z = int(input("Digite 1 para adicionar um produto e 2 para finalizar: "))
			if z == 2: 
				break
			if z != 1: 
				raise TypeError
			x = float(input("Qual o valor? "))
			y = float(input("Qual a porcentagem de desconto (0-100): ")) 
			if x<0 or y<0: 
				raise NameError
			a += x
			b += x*((100-y)/100)
		except NameError: 
			print("Digite somente números positivos. \n")
		except TypeError:
			print("Digite somente 1 ou 2.\n")
		except ValueError:
			print("Digite somente números.\n")
	return print("Você pagaria: ", a, "Você vai pagar: ", b)


loja()
	